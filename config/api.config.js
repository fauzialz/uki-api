require('dotenv').config()

module.exports = {
    PORT: process.env.SERVER_PORT || 3000,
    SERVER: {
        NAME: process.env.SERVER_NAME || 'UKI Public API',
        VERSION: process.env.SERVER_VERSION || '1.0.0'
    },
    DB: {
        URL: process.env.MONGODB_URL || 'mongodb://@localhost',
        PORT: process.env.MONGODB_PORT || 27017,
        NAME: process.env.MONGODB_NAME || 'db_UKI'
    }
}