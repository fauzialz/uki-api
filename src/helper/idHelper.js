Number.prototype.pad = function (size) {
    var s = String(this)
    while (s.length < (size || 2)) {s = "0" + s}
    return s
}

const idHelper = {
    employee: (emData) => {
        if(emData.length>0){
            let lastId = emData[0].id
            let lastNum = parseInt(lastId.replace('E',''))
            
            lastNum += 1
            let padNum = (lastNum).pad(2)
            return ('E' + padNum)
            /* emData.id = genCode */
        }else{
            return 'E01'
        }
    }
    //WE CAN ADD MORE FUNCTION FOR MORE DB COLLECTION or FIND ANOTHER METHOD
}

module.exports = idHelper