const emModel = require('./employeeModel')
const idHelper = require('../../helper/idHelper')

const employeeController = {
    readAllEmployee: async (req, res, next) => {
        const { db } = global
        await db.collection('employee').find().toArray((err, docs) => {
            if(docs){
                res.send(200, {
                    code: 200,
                    message: 'This is employees.',
                    data: docs
                })
            }else{
                res.send(507, {
                    code: 507,
                    message: 'Trouble with database.',
                })
            }
        })
    },
    readEployeeById: async (req, res, next) => {
        const { db } = global
        await db.collection('employee').find({ id : req.params.id }).toArray((err, doc) => {
            if(doc.length>0){
                res.send(200, {
                    code: 200,
                    message: 'This is employee with id : '+req.params.id,
                    data: doc
                })
            }else{
                res.send(400, {
                    code: 400,
                    message: "Can't find the data in database"
                })
            }
        })
    },
    createEmployee: async (req, res, next) => {
        const { db } = global
        let emData = req.body
        /* console.log(emData) */
        if(emData.name && emData.department){
            emData = new emModel(emData)
            await db.collection('employee').findOne({ name: emData.name }, (err, here) => {
                if(here){
                    res.send(400, {
                        code : 400,
                        message : 'Employee already exist.'
                    })
                }else{
                    db.collection('employee').find().limit(1).sort({ $natural: -1 }).toArray((err, docs) => {
                        emData.id = idHelper.employee(docs)
                
                        db.collection('employee').insertOne(emData, (err, doc) => {
                            if(!err){
                                res.send(200, {
                                    code: 200,
                                    message: "New employee data has been added.",
                                    doc
                                })
                            }else{
                                console.log(err)
                                res.send(507, {
                                    code : 507,
                                    message : 'Fail to insert new data.',
                                    err 
                                })
                            }
                        })
                    })
                }
            })     
        }else{
            res.send(400, {
                code : 400,
                message : 'No proper input.'
            })
        }
    },
    editEmployee: async (req, res, next) => {
        const { db } = global
        let emData = req.body
        if((emData.name && emData.department) && req.params.id){
            await db.collection('employee').findOne({ id: req.params.id }, (err, here) => {
                if((here.name == emData.name) && (here.department == emData.department)){
                     res.send(400, {
                        code : 400,
                        message : 'You input the same data.'
                    })
                }else{
                    emData = {
                        $set: {
                            name: emData.name,
                            department: emData.department
                        }
                    }
                    db.collection('employee').updateOne(
                        { id: req.params.id }, emData, (err, doc) => {
                            if(!err){
                                res.send(200, {
                                    code: 200,
                                    message: "Employee data has been edited.",
                                    doc
                                })
                            }else{
                                console.log(err)
                                res.send(507, {
                                    code : 507,
                                    message : 'Fail to edit the data.',
                                    err 
                                })
                            }
                        }
                    )
                }
            })
        }else{
            res.send(400, {
                code : 400,
                message : 'No proper input.'
            })
        }
    },
    deleteEmployee: async (req, res, next) => {
        const { db } = global
        db.collection('employee').deleteOne({ id: req.params.id}, (err, doc) => {
            if(!err){
                res.send(200, {
                    code: 200,
                    message: "Employee data has been deleted.",
                    doc
                })
            }else{
                console.log(err)
                res.send(507, {
                    code : 507,
                    message : 'Fail to delete the data.',
                    err 
                })
            }
        })
    }
}

module.exports = employeeController