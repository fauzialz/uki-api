const restify = require('restify')
const corsMW = require('restify-cors-middleware')
const config = require('./config/api.config')
const DBConnection = require('./src/helper/dbHelper')

//CREATE SERVER WITH RESTIFY
const server = restify.createServer({
    name: config.SERVER.NAME,
    version: config.SERVER.VERSION
})
const cors = corsMW({
    origins: ['*'],
    allowHeaders: ['*'],
    exposeHeaders: ['*']
})
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())
server.pre(cors.preflight)
server.use(cors.actual)

//CONNECT TO MONGODB
DBConnection.connect((err, db) => {
    if(err){
        console.log(err)
        process.exit()
    }else{
        console.log('\x1b[36m%s\x1b[1m','\n[DATABASE] is connected...')
    }
})

//SET THE ROUTES
require('./src/routes')(server)

//RUNING THE SERVER
server.listen(config.PORT, () => {
    console.log('\x1b[33m%s\x1b[1m','\n[SERVER] is running...\nName : '+config.SERVER.NAME+'\nPort : '+config.PORT)
})

